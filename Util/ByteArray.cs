﻿using System;
using System.IO;
using System.Text;

namespace Bionis.Util
{
    public class ByteArray
    {
        MemoryStream stream;
        BinaryWriter writer;
        BinaryReader reader;
        public ByteArray(int length)
        {
            stream = new MemoryStream(length);
            writer = new BinaryWriter(stream);
            reader = new BinaryReader(stream);
        }
        public void Write(byte[] data, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(data);
        }
        public void Write(string value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(Encoding.ASCII.GetBytes(value));
        }
        public void Write(byte value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(value);
        }
        public void WriteLE(ushort value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)value, (byte)(value >> 8) });
        }
        public void WriteLE(int value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)value, (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24) });
        }
        public void WriteLE(uint value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)value, (byte)(value >> 8), (byte)(value >> 16), (byte)(value >> 24) });
        }
        public void WriteBE(ushort value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)(value >> 8), (byte)value });
        }
        public void WriteBE(uint value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)(value >> 24), (byte)(value >> 16), (byte)(value >> 8), (byte)value });
        }
        public void WriteBE(int value, int position = 0)
        {
            writer.Seek(position, SeekOrigin.Begin);
            writer.Write(new byte[] { (byte)(value >> 24), (byte)(value >> 16), (byte)(value >> 8), (byte)value });
        }
        public byte[] GetBytes()
        {
            return stream.GetBuffer();
        }
    }
}
