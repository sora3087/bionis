using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bionis.Network;
using System.Text;
namespace NetworkTests
{
    [TestClass]
    public class CryptoTests
    {
        [TestMethod]
        public void EncryptionValidates()
        {
            //uint serverVector = 0x747a4d96;
            uint serverVector = 0x964d7a74;

            Crypto serverEncryption = new Crypto();
            serverEncryption.CreateKeys(serverVector);

            byte[] welcomeACK = { 0x04, 0x00, 0x04, 0x00 };

            serverEncryption.CryptData(welcomeACK, 0, 4);

            StringBuilder hex = new StringBuilder(welcomeACK.Length * 2);
            foreach (byte b in welcomeACK)
                hex.AppendFormat("{0:x2}", b);

            Assert.AreEqual<byte[]>(new byte[] { 0xba, 0x75, 0x1c, 0x5e }, welcomeACK, "actual {0}", hex.ToString());
        }
    }
}
