﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net;
using Bionis.Network;
using Bionis.Util;
using RazorLight;

namespace Bionis.Patch
{
    public class WelcomeMessageModel {
        public string User = "";
    }
    class App
    {
        private List<PatchEntry> PatchEntries = new List<PatchEntry>();
        private Dictionary<EndPoint, Session> Sessions = new Dictionary<EndPoint, Session>();
        private CommandRouter router = new CommandRouter();
        private void CreatePatchEntries()
        {
            if (!Directory.Exists("./patches"))
            {
                Console.WriteLine("Patch directory doesn't exist. Creating...");
                Directory.CreateDirectory("./patches");
            }
            string[] files = Directory.GetFiles("./patches", "*", SearchOption.AllDirectories);
            if(files.Length == 0)
            {
                Console.WriteLine("At least one patch file is needed. Creating...");
                FileStream dummy = File.Create("./patches/dummy.txt");
                dummy.Write(Encoding.ASCII.GetBytes("dummy"));
                dummy.Close();
                Array.Resize(ref files, files.Length + 1);
                files[files.Length - 1] = "./patches/dummy.txt";
            }
            Console.WriteLine("Scanning patch files...\n");
            for (int i = 0; i < files.Length; i++)
            {
                if(files[i].Length <= 48)
                {
                    PatchEntry entry = new PatchEntry();

                    entry.path = files[i].Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    entry.name = Path.GetFileName(entry.path);
                    entry.folder = Path.GetDirectoryName(entry.path).Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
                    entry.folders = entry.folder.Split(Path.AltDirectorySeparatorChar);
                    bool skip = false;
                    for (int j = 0; j < entry.folders.Length; j++)
                    {
                        if (entry.folders[j].Length > 64)
                        {
                            skip = true;
                        }
                    }
                    if(!skip)
                    {
                        byte[] data = File.ReadAllBytes(entry.path);
                        entry.checksum = CRC32.Hash(data, 0, data.Length);
                        Console.WriteLine(entry.path + " " + entry.checksum.ToString("X"));
                        PatchEntries.Add(entry);
                    }
                    else
                    {
                        Console.WriteLine("Dirname too long: " + files[i]);
                    }
                }
                else
                {
                    Console.WriteLine("Filename too long: " + files[i]);
                }
                

                //byte[] data = File.ReadAllBytes(files[i]);
                //int fileLength = data.Length;
                //uint hash = CRC32.Hash(data, 0, fileLength);
                //Console.WriteLine(files[i] + " " + hash.ToString("X"));
            }
        }
        public void Start()
        {
            CreatePatchEntries();

            CreateRoutes();

            Server Patch = new Server();
            Patch.OnConnect(PatchConnect);
            Patch.Listen(11000, "192.168.1.142");

            Server Data = new Server();
            Data.OnConnect(DataConnect);
            Data.Listen(11001, "192.168.1.142");
        }
        public void CreateRoutes()
        {
            router.Command(0x02, WelcomeAck);
            router.Command(0x04, ClientSendingLogin);
            router.Command(0xB1, DebugMessage);
            router.Command(0x29, DebugMessage);
        }
        public void WelcomeAck(Session session, byte[] data)
        {
            session.Write(new byte[] { 0x04, 0x00, 0x04, 0x00 });
        }
        public void ClientSendingLogin(Session session, byte[] data)
        {
            // TODO I don't think we care about login validation here
            // Tetehalla seems to respond with welcome message regardless of proper auth

            SendWelcomeText(session, data);
        }
        public async void SendWelcomeText(Session session, byte[] data)
        {
            var engine = new RazorLightEngineBuilder()
              .UseMemoryCachingProvider()
              .Build();

            string template = "Hello, @Model.User. \tC3Welcome to \tC1Bionis patch server!";
            WelcomeMessageModel model = new WelcomeMessageModel() { User = "Player" };

            string result = await engine.CompileRenderAsync("Welcome", template, model);

            result = result.PadRight(result.Length + result.Length % 4, '\0');

            byte[] message = Encoding.Unicode.GetBytes(result);

            byte[] response = new byte[message.Length + 4];
            response[0] = (byte)response.Length;
            response[1] = (byte)(response.Length >> 8);
            response[2] = 0x13;
            message.CopyTo(response, 0x4);
            session.Write(response);

            SendDataRedirect(session, data);
        }
        public void SendDataRedirect(Session session, byte[] data)
        {
            ByteArray message = new ByteArray(0xC);
            message.WriteLE((ushort)0xC, 0x0);
            message.WriteLE((ushort)0x14, 0x2);
            // TODO get current ip address

            byte[] address = IPAddress.Parse("192.168.1.142").GetAddressBytes();
            
            message.Write(address, 0x4);
            message.WriteBE((ushort)11001, 0x8);
            // session.EndAfterNextSend();
            session.Write(message.GetBytes());
        }
        public void DebugMessage(Session session, byte[] data)
        {

        }
        private void PatchConnect(EventSocket socket)
        {
            Console.WriteLine("PATCH connection from {0}", socket.RemoteEndPoint.ToString());
            Session session = new Session(socket, router);
            session.SendHandshake();
        }
        private void DataConnect(EventSocket socket)
        {
            Console.WriteLine("DATA connection from {0}", socket.RemoteEndPoint.ToString());
            Session session = new Session(socket, router);
            session.SendHandshake();
        }
    }
}
