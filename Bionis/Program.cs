﻿using System;
using System.Threading;
using System.Threading.Tasks;
namespace Bionis.Patch
{
    class Program
    {
        static ManualResetEvent resetEvent = new ManualResetEvent(false);
        static async Task Main(string[] args)
        {   
            App s = new App();
            s.Start();
            resetEvent.WaitOne(); 
        }
    }
}
