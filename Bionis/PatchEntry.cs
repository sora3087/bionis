﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bionis.Patch
{
    class PatchEntry
    {
        public uint checksum;
        public string name;
        public string folder;
        public string[] folders;
        public string path;
        public int size;
    }
}
