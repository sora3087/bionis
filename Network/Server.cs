﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;

namespace Bionis.Network
{
    public class Server
    {
        private IPEndPoint EndPoint;
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        private static List<Action<EventSocket>> ConnectionListeners = new List<Action<EventSocket>>();
        private Socket server;
        public void Listen(int port)
        {
            IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddress = ipHostInfo.AddressList[0];
            Listen(port, ipAddress);
        }
        public void Listen(int port, string host)
        {
            Listen(port, IPAddress.Parse(host));
        }
        public void Listen(int port, IPAddress host)
        {
            EndPoint = new IPEndPoint(host, port);
            Start(host, port);
        }
        public void OnConnect(Action<EventSocket> callback) {
            Console.WriteLine("bind callback");
            ConnectionListeners.Add(callback);
        }
        private void Start(IPAddress host, int port) {
            Console.WriteLine("Waiting for connections on {0}:{1}", host.ToString(), port);
            server = new Socket(host.AddressFamily,
            SocketType.Stream, ProtocolType.Tcp);
            try
            {
                server.Bind(EndPoint);
                server.Listen(100);
                server.BeginAccept(new AsyncCallback(AcceptConnection), server);
            }
            catch (SocketException e)
            {
                Console.WriteLine("Cannot bind to port {0}", port);
            }
        }
        private void AcceptConnection(IAsyncResult asyncResult)
        {
            Socket listener = (Socket)asyncResult.AsyncState;
            Socket handler = listener.EndAccept(asyncResult);

            EventSocket eventSocket = new EventSocket(handler);
            ConnectionListeners.ForEach(delegate (Action<EventSocket> callback) {
                callback(eventSocket);
            });
            // start listening for next connection
            server.BeginAccept(new AsyncCallback(AcceptConnection), server);
        }
    }
}
