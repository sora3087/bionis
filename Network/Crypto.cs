﻿namespace Bionis.Network
{
    public class Crypto
    {

        private uint[] keys;
        private uint position;

        public Crypto()
        {
            this.keys = new uint[56];
        }

        public void CreateKeys(uint IV)
        {
            uint index, key2;

            key2 = 1;
            this.keys[55] = IV;

            for (int i1 = 0x15; i1 <= 0x46E; i1 += 0x15)
            {
                index = (uint)(i1 % 55);
                IV -= key2;
                this.keys[index] = key2;
                key2 = IV;
                IV = this.keys[index];
            }

            MixKeys();
            MixKeys();
            MixKeys();
            MixKeys();
            this.position = 56;
        }
        private void MixKeys()
        {
            for (int i1 = 0x18, i2 = 0x01; i1 > 0; i1--, i2++)
            {
                this.keys[i2] -= this.keys[i2 + 0x1F];
            }
            for (int i1 = 0x1F, i2 = 0x19; i1 > 0; i1--, i2++)
            {
                this.keys[i2] -= this.keys[i2 - 0x18];
            }
        }

        public void CryptData(byte[] data, int index, int length)
        {
            length += index;
            for (int i1 = index; i1 < length; i1 += 4)
            {
                CryptU32(data, i1);
            }
        }
        private void CryptU32(byte[] data, int offset)
        {
            if (this.position == 56)
            {
                MixKeys();
                this.position = 1;
            }

            data[offset + 0] ^= (byte)(this.keys[this.position] >> 0);
            data[offset + 1] ^= (byte)(this.keys[this.position] >> 8);
            data[offset + 2] ^= (byte)(this.keys[this.position] >> 16);
            data[offset + 3] ^= (byte)(this.keys[this.position] >> 24);
            this.position++;
        }
    }
}
