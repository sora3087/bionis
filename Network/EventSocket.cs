﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace Bionis.Network
{
    public class EventSocket
    {
        private Socket socket;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
        private IEnumerable<byte> output = new byte[BufferSize];
        private static List<Action<byte[]>> DataListeners = new List<Action<byte[]>>();
        private static List<Action> DisconnectListeners = new List<Action>();
        public bool disconnectNext = false;
        public EventSocket(Socket connection)
        {
            socket = connection;
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);
            socket.BeginReceive(buffer, 0, BufferSize, SocketFlags.None, new AsyncCallback(ReadData), this);
        }
        public void Write(byte[] data)
        {
            socket.BeginSend(data, 0, data.Length, 0,  
            new AsyncCallback(SendData), socket);
        }
        public void OnData(Action<byte[]> action)
        {
            DataListeners.Add(action);
        }
        public void OnDisconnect(Action action)
        {
            DisconnectListeners.Add(action);
        }
        private void SendData(IAsyncResult asyncResult)
        {
            int bytesSent = socket.EndSend(asyncResult);
            Console.WriteLine("Sent {0} bytes to client.", bytesSent);
            if (disconnectNext)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            else
            {
                socket.BeginReceive(buffer, 0, BufferSize, SocketFlags.None, new AsyncCallback(ReadData), this);
            }
        }
        private void ReadData(IAsyncResult asyncResult)
        {
            if (!disconnectNext)
            {
                int bytesRead = socket.EndReceive(asyncResult);

                // this code inherited from MS assumes that the client sets an end write mode or EOF
                // PSO may not do this and would need other ways to determine that sending has stopped
                if (bytesRead > 0)
                {
                    //output = output.Concat(buffer);
                    DataListeners.ForEach(delegate (Action<byte[]> callback) {
                        byte[] output = new byte[bytesRead];
                        Array.Copy(buffer, 0, output, 0, bytesRead);
                        callback(output);
                    });
                }
                socket.BeginReceive(buffer, 0, BufferSize, SocketFlags.None, new AsyncCallback(ReadData), this);
            }
        }
        public EndPoint RemoteEndPoint
        {
            get { return socket.RemoteEndPoint; }
        }
        public void Disconnect()
        {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
        }
    }
}
