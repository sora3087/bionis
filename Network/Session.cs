﻿using System;
using System.Linq;
using Bionis.Util;

namespace Bionis.Network
{
    public class Session
    {
        private EventSocket socket;
        private CommandRouter router;
        
        private Boolean encrypted = false;

        private Crypto clientEncryption;
        private Crypto serverEncryption;

        public Session(EventSocket eventSocket, CommandRouter commandRouter)
        {
            socket = eventSocket;
            router = commandRouter;
            clientEncryption = new Crypto();
            serverEncryption = new Crypto();
            socket.OnData(OnData);
            socket.OnDisconnect(OnDisconnect);
        }

        public void Write(byte[] data)
        {
            if (encrypted)
            {
                // TODO pad bytes out to increment of 4
                serverEncryption.CryptData(data, 0, data.Length);
            }
            socket.Write(data);
        }

        public void OnData(byte[] data)
        {
            if (encrypted)
            {
                // TODO pad bytes out to increment of 4
                clientEncryption.CryptData(data, 0, data.Length);
            }
            Console.WriteLine("Received {0} bytes from client {1}", data.Length, socket.RemoteEndPoint.ToString());
            ushort DataLength = BitConverter.ToUInt16(data);

            router.Process(this, data.Skip(2).ToArray());
        }
        private void OnDisconnect()
        {
        }
        public void SendHandshake()
        {
            ByteArray message = new ByteArray(0x4c);
            // Message length
            message.WriteLE((ushort)0x4c, 0x0);
            // Message code
            message.WriteLE((ushort)0x02, 0x2);
            // Message authority
            message.Write("Patch Server. Copyright SonicTeam, LTD. 2001", 0x4);

            // Create random seed
            TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
            Random rand = new Random((int)t.TotalSeconds);

            //uint serverVector = (uint)rand.Next();
            uint serverVector = 0x964d7a74;
            uint clientVector = (uint)rand.Next();

            // Write encryption vectors to message
            message.WriteLE(serverVector, 0x44);
            message.WriteLE(clientVector, 0x48);

            // Create keys from encryption vectors
            serverEncryption.CreateKeys(serverVector);
            clientEncryption.CreateKeys(clientVector);

            byte[] data = message.GetBytes();

            Write(data);
            // Turn on encryption for all further communication
            encrypted = true;
        }
        public void EndAfterNextSend()
        {
            socket.disconnectNext = true;
        }
    }
}
