﻿using System;
using System.Collections.Generic;

namespace Bionis.Network
{
    public class CommandRouter
    {
        private Dictionary<int, Action<Session, byte[]>> stack = new Dictionary<int, Action<Session, byte[]>>();
        public CommandRouter()
        {

        }
        public void Process(Session session, byte[] message)
        {
            byte command = message[0];
            byte flags = message[1];
            if (stack.ContainsKey(command))
            {
                stack[command](session, message);
            }
            else
            {
                Console.WriteLine("Tried routing command {0:X} but nothing was registered", command);
            }
        }
        public void Command(int code, Action<Session, byte[]> action)
        {
            stack.Add(code, action);
        }
        public void Command(int code, CommandRouter router)
        {
            stack.Add(code, router.Process);
        }
    }
}
